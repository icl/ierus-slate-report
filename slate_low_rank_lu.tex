\documentclass[11pt,letterpaper]{report}
\input{config.tex}
\setenumerate{label=(\arabic*),topsep=0ex,itemsep=0ex,partopsep=1ex,parsep=1ex}

% math functions
\newcommand{\rank}{\text{rank}}

\begin{document}

\pagenumbering{roman}
\tableofcontents
\listoffigures
% \listoftables
\pagestyle{fancy}
\setlength\parindent{0in}
\setlength\parskip{0.1in}
\sloppy
\rm

\clearpage
\pagenumbering{arabic}
%===============================================================================
\chapter{SLATE}
\label{sec:slate}
SLATE (Software for Linear Algebra Targeting Exascale)~\footnote{\url{http://icl.utk.edu/slate/}}
is being developed as part of the Exascale Computing
Project~(ECP)~\footnote{\url{https://www.exascaleproject.org}},
which is a joint project of the U.S. Department of Energy's Office of Science
and National Nuclear Security Administration (NNSA).
SLATE will deliver fundamental dense linear algebra capabilities
for current and upcoming distributed-memory systems, including GPU-accelerated systems as well as more traditional
multi core--only systems.

%
SLATE provides coverage of existing ScaLAPACK functionality, including
the parallel BLAS; linear systems using LU and Cholesky; least squares
problems using QR; and eigenvalue and singular value problems.
%
SLATE is designed as a replacement for ScaLAPACK, which after two
decades of operation, cannot adequately be retrofitted for modern
accelerated architectures.
%
SLATE uses modern techniques such as communication-avoiding
algorithms, lookahead panels to overlap communication and computation,
and task-based scheduling, along with a modern C++ framework.
%
The SLATE project website is located at:\\
\url{https://icl.utk.edu/slate/}

The SLATE software can be downloaded from:\\
\url{https://bitbucket.org/icl/slate/}

The SLATE auto-generated function reference can be found at:\\
\url{https://icl.bitbucket.io/slate/}

%----------------------------------------
\section{Functionality and Goals of SLATE}

SLATE operates on dense matrices, solving systems of linear
equations, linear least squares problems, eigenvalue problems, and
singular value problems. SLATE also handles many associated
computations such as matrix factorizations and matrix norms.
SLATE routines also support distributed parallel band factorization
and solve and the associated band routines.

SLATE is intended to fulfill the following design goals:
\begin{description}
%
\item[Targets modern HPC hardware]
consisting of a large number of
nodes with multicore processors and several hardware accelerators per node.
%
% \item[Achieves maximum performance]
% by relying on vendor's highly optimized implementations of BLAS, batched BLAS, and
% LAPACK, including accelerated versions.
% (Intel MKL~\cite{mkl}, IBM ESSL~\cite{essl}, Cray LibSci~\cite{libsci},
% NVIDIA cuBLAS~\cite{cublas}, AMD rocBLAS~\cite{rocblas}, etc.).
%
\item[Achieves portable high performance]
by relying on vendor optimized standard BLAS, batched BLAS,
and LAPACK, and standard parallel programming technologies such as MPI and OpenMP.
Using the OpenMP runtime puts less of a burden on applications to
integrate SLATE than adopting a proprietary runtime would.
%
\item[Provides scalability]
by employing proven techniques in dense linear algebra,
such as 2D block cyclic data distribution and communication-avoiding algorithms,
as well as modern parallel programming approaches,
such as dynamic scheduling and communication overlapping.
%
\item[Facilitates productivity]
by relying on the intuitive \emph{Single Program, Multiple Data}~(SPMD)
programming model and a set of simple abstractions to represent dense
matrices and dense matrix operations.
%
\item[Assures maintainability]
by employing useful C++ facilities, such as templates
and overloading of functions and operators, with a focus on minimizing code.
%
\item[Eases transition to SLATE]
by natively supporting the ScaLAPACK 2D block-cyclic layout and providing a
backwards-compatible ScaLAPACK API.
\end{description}

SLATE uses
\emph{TestSweeper}~\footnote{\url{https://bitbucket.org/icl/testsweeper}},
a modern testing framework that can
exercise much of the functionality provided by the library.  This
framework sweeps over an input space of parameters to check valid
combinations of parameters when calling SLATE routines.


%----------------------------------------
\section{Software Components Required by SLATE}
% BLAS++, LAPACK++, MPI, OpenMP, Batched BLAS}
%
\begin{figure}
    \centering
    \includegraphics[width=0.55\textwidth]{figures/slate-layers}
    \caption{Software layers in SLATE.}
    \label{fig:layers}
\end{figure}
%
SLATE builds on top of a small number of component packages, as depicted
in \cref{fig:layers}.  The BLAS++ library provides overloaded
C++ wrappers around the Fortran BLAS routines, exposing a
single function interface to a routine independent of the datatype of
the operands.  The BLAS++ provides both column-major and
row-major access to matrices with no-to-minimal performance overhead.
The BLAS++ library provides Level-1, 2, and 3 BLAS on
the CPU, and Level-3 BLAS on GPUs (via CUDA's cuBLAS at
present).  If Level-3 Batched BLAS routines exist, they are provided
on the CPU and GPU as well.

The LAPACK++ library provides similar datatype independent
overloaded C++ wrappers around the Fortran LAPACK routines.
Note that LAPACK++ provides support only for column-major
access to matrices.  The LAPACK++ wrappers allocate optimal
workspace sizes as needed by the routines, so that the user is not
required to allocate workspace in advance.

Within a process, multi-threading is obtained using OpenMP constructs.
More specifically, OpenMP task-depend clauses are used to specify high
level dependencies in SLATE algorithms and OpenMP task-parallelism is
used to distribute data parallel work to the processors.

Efficient use of GPUs and accelerators is obtained by using the Batched
BLAS API.  The Batched BLAS is an emerging standard technique for
aggregating smaller BLAS operations to efficiently use the hardware
and obtaining higher performance.

MPI is used for communication between processes in SLATE.
Communication in SLATE relies on explicit dataflow information.  When
tiles are needed for computation, they are broadcast to all the
processes where they are required.
%~\ref{fig:chol-bcast} shows a.
%single tile being broadcast from the Cholesky panel to a block row and.
%block column for the trailing matrix update.  The broadcast is.
%expressed in terms of the destination tiles, which are internally.
%mapped by SLATE to explicit MPI ranks..
%\begin{figure}[bt].
%    \centering.
%    \includegraphics[width=0.25\textwidth]{figures/matrix6-chol-bcast}.
%    \caption{Broadcast of tile and its symmetric image to nodes owning a.
%    block row and block column in a symmetric matrix.}.
%    \label{fig:chol-bcast}.
%\end{figure}

%----------------------------------------
\section{Foundations of SLATE's Design}
This section outlines the basis of slate's design.
\begin{description}
\item[Object-Oriented Design:]
The design of SLATE revolves around the Tile class and the Matrix
class hierarchy. The Tile class is intended as a simple class
for maintaining the properties of individual tiles and implementing
core serial tile operations, such as tile BLAS,
while the Matrix class hierarchy maintains the state of distributed
matrices throughout the execution of parallel matrix algorithms
in a distributed-memory environment.
Currently, the classes are structured as follows:

\begin{description}
\item[BaseMatrix] is an abstract base class for all matrices.
    \begin{description}
    \item[Matrix] represents a general $m \times n$ matrix.

    \item[BaseTrapezoidMatrix] is an abstract base class for all
        upper-trapezoid or lower-trapezoid, $m \times n$ matrices.
        For upper matrices, tiles $A(i,j)$ are stored for $i \le j$.
        For lower matrices, tiles $A(i,j)$ are stored for $i \ge j$.

        \begin{description}
        \item[TrapezoidMatrix] represents an upper-trapezoid
            or a lower-trapezoid, $m \times n$ matrix.
            The opposite triangle is implicitly zero.

            \begin{description}
            \item[TriangularMatrix] represents an upper-triangular
                or a lower-triangular, \mbox{$n \times n$} matrix.
            \end{description}

        \item[SymmetricMatrix] represents a symmetric, $n \times n$ matrix,
            with only the upper or lower triangle stored.
            The opposite triangle is implicitly known by symmetry
            \mbox{($A_{j,i} = A_{i,j}$)}.

        \item[HermitianMatrix] represents a Hermitian, $n \times n$ matrix,
            with only the upper or lower triangle stored.
            The opposite triangle is implicitly known by symmetry
            \mbox{($A_{j,i} = \bar A_{i,j}$)}.

        \end{description}
    \end{description}
\end{description}

\item[Tiled Matrix Layout:]
The new matrix storage introduced in SLATE is one of its most impactful
features. In this respect, SLATE represents a radical departure from other
distributed linear algebra software such as ScaLAPACK or Elemental,
where the local matrix occupies a contiguous memory region on each process.
In contrast, tiles are first class objects in SLATE that can be individually
allocated and passed to low-level tile routines. In SLATE, the matrix consists
of a collection of individual tiles with no correlation between their positions
in the matrix and their memory locations. At the same time, SLATE also supports
tiles pointing to data in a traditional ScaLAPACK matrix layout,
thereby easing an application's transition from ScaLAPACK to SLATE.

\item[Handling of side, uplo, trans:]
The classical BLAS takes parameters such as \verb|side|, \verb|uplo|, \verb|trans|
(named ``\verb|op|'' in SLATE), and \verb|diag| to specify operation variants.
Traditionally, this has meant that implementations have numerous cases.
The reference BLAS has nine cases in \verb|zgemm| and eight cases in \verb|ztrmm|
(times several sub-cases).
ScaLAPACK and PLASMA likewise have eight cases in \verb|ztrmm|.
In contrast, by storing both \verb|uplo| and \verb|op| within the matrix object itself,
and supporting inexpensive shallow copy transposition, SLATE can implement
just one or two cases and map all the other cases to that implementation
by appropriate transpositions.
For instance, SLATE only implements one case for \verb|gemm|
(\verb|NoTrans, NoTrans|) and handles all other cases
by swapping indices of tiles and setting \verb|trans| appropriately for the
underlying tile operations.

\item[Templating of Precisions:]
SLATE handles multiple precisions by C++ templating,
so there is only one precision-independent version of the code,
which is then instantiated for the desired precisions.
Operations are defined so that they can be applied consistently across all precisions.
SLATE's BLAS++ component provides overloaded, precision-independent wrappers
for all underlying, node-level BLAS, and SLATE's PBLAS are built on top of these.
Currently, the SLATE library has explicit instantiations of the four main
data types: \verb|float|, \verb|double|, \verb|std::complex<float>|,
and \verb|std::complex<double>|.
The SLATE code should be able to accommodate other data types, such as half,
double-double, or quad precision, given appropriate underlying node-level BLAS.

\item[Templating of Execution Targets:]
Parallelism is expressed in SLATE's computational routines.
Each computational routine solves a sub-problem,
such as computing an LU factorization (\verb|getrf|)
or solving a linear system given an LU factorization (\verb|getrs|).
In SLATE, these routines are templated for different targets (CPU or GPU),
with the code typically independent of the target.
The user can choose among various target implementations:
\begin{description}
\item[Target::HostTask] means multithreaded execution
    by a set of OpenMP tasks.
\item[Target::HostNest] means multithreaded execution
    by a nested ``\verb|parallel for|'' loop.
\item[Target::HostBatch] means multithreaded execution
    by calling a batched BLAS routine.
\item[Target::Devices] means (multi-)GPU execution using calls to batched BLAS.
\end{description}

\item[MPI Communication:]
Communication in SLATE relies on explicit dataflow information.
When a tile is needed for computation, it is broadcast to all the processes
where it is required.
Rather than explicitly listing MPI ranks, the broadcast is expressed in terms
of the destination (sub)matrix to be updated.
This way, SLATE's messaging layer is oblivious to the mapping of tiles
to processes.
Also, multiple broadcasts are aggregated to allow for pipelining
of MPI messages with transfers between the host and the devices.
Since the set of processes involved in a broadcast is determined dynamically,
the use of MPI collectives is not ideal, as it would require setting up a new
subcommunicator for each broadcast.
Instead, SLATE uses point-to-point MPI communication following a hypercube
pattern to broadcast the data.

\item[Node-Level Coherency:]
For offload to GPU accelerators, SLATE implements a memory consistency model,
inspired by the MOSI cache coherency
protocol~\cite{sweazey1986class,sorin2011primer}, on a tile-by-tile basis.
For read-only access, tiles are mirrored in the memories of, possibly multiple,
GPU devices and deleted when no longer needed.
For write access, tiles are migrated to the GPU memory and returned
to the CPU memory afterwards if needed.
A tile's instance can be in one of three states:
{\em Modified}, {\em Shared}, or {\em Invalid}.
Additional flag {\em OnHold} can be set along any state, as follows:
\begin{description}
\item[Modified (M)] indicates that the tile's data is modified.
    Other instances should be {\em Invalid}.
    The instance cannot be purged.

\item[Shared (S)] indicates that the tile's data is up-to-date.
    Other instances may be {\em Shared} or {\em Invalid}.
    The instance may be purged unless it is on hold.

\item[Invalid (I)] indicates that the tile's data is obsolete.
    Other instances may be {\em Modified}, {\em Shared}, or {\em Invalid}.
    The instance may be purged unless it is on hold.

\item [OnHold (O)] is a flag orthogonal to the other three states that
    indicates a hold is set on the tile instance,
    and the instance cannot be purged until the hold is released.
\end{description}

\item[Dynamic Scheduling:]
Dataflow scheduling (\verb|omp task depend|) is used to execute
a task graph with nodes corresponding to large blocks of the matrix.
Dependencies are tracked using dummy vectors, where each element represents
a block of the matrix, rather than the matrix data itself.
For multi-core execution, each large block is dispatched to multiple cores---using
either nested tasking (\verb|omp task|) or batched BLAS.
For GPU execution, calls to batched BLAS are used specifically to deliver
fast processing of matrix blocks that are represented as large collections
of tiles.
\end{description}

%===============================================================================
\chapter{Implementation Details}
\label{ch:impl}

This chapter describes the implementation details required to solve $Ax=b$,
where $A$ is a matrix with low-rank tiles and non-uniform tile sizes.
We implement the driver routine \verb|gesv_diag_piv_UV| to solve the entire problem $Ax=b$.
The \verb|gesv_diag_piv_UV| routine in turn relies on computational routines \verb|getrf_diag_piv_UV|
to compute the $LU$ factorization of the matrix $A$ (\cref{sec:getrf}), followed by
\verb|getrs_diag_piv| to solve the linear systems using the computed $L$ and $U$ factors
(\cref{sec:getrs}).
The \verb|gesv_diag_piv_UV| driver routine is independent of the target (CPU or device),
delegating those details to lower level routines.

%----------------------------------------
\section{Matrix with Non-Uniform Tiles}
\label{sec:nonu-tile}

SLATE can support non-uniform tile sizes within a matrix (\cref{fig:matrix-rectangular}).
Most factorizations require
that the diagonal tiles are square, but the block row heights and block column
widths can, in principle, be arbitrary.
When constructing the matrix, the user provides functions
\lstinline|tileMb( i )| and  \lstinline|tileNb( j )| that tell SLATE the
height of block row $i$ and width of block column $j$,
respectively.
This facilitates applications where the block structure is significant,
for instance in \emph{Adaptive Cross Approximation} (ACA).

\begin{figure}
    \centering
    \includegraphics[width=0.24\textwidth]{figures/matrix3-rectangular}
    \caption{Block sizes can vary. Most algorithms require square diagonal tiles.}
    \label{fig:matrix-rectangular}
\end{figure}

%----------------------------------------
\section{Low-rank Restricted Pivoting LU}
\label{sec:getrf}

We develop the new $LU$ factorization of matrices with compressed tiles, the \verb|getrf_diag_piv_UV| routine.
%The \verb|getrf_diag_piv_UV| performs the pivoting on the diagonal tiles only.
\Cref{alg:getrf} illustrates the major steps of this algorithm.
Each tile can be represented as $T_{ij} = U_{ij}V_{ij}$, where $i$ and $j$ are
the block row and column position, respectively, of the tile.
\Cref{fig:getrf} shows the main steps of one iteration ($k=0$) of \cref{alg:getrf}, using
matrix of size $3 \times 3$ tiles, with full diagonal tiles and compressed off-diagonal tiles.
First the inverse of the diagonal tiles is computed by calling
\verb|lapack::getrf| and \verb|lapack::getri| (red tile), followed by updating the block
column below the diagonal tile (blue tiles) by multiplying it by the inverse
of the diagonal tile, and then updating the trailing submatrix (green tiles)
by forming a block outer product of column $k$ (blue tiles) and row $k$ (yellow tiles).

The low-rank LU algorithm necessitates a new
matrix-matrix multiplication kernel that takes into account the data format (i.e., full or low-rank).
Therefore, we implement \verb|slate::internal::gemm_compressed| to perform on various tiles.
The \verb|getrf_diag_piv_UV| calls a sequence of \verb|slate::internal::gemm_compressed|,
each of which performs one block outer product.
The implementation details of \verb|internal::gemm_compressed|
are shown in \cref{sec:internal-gemm-comp}.
The \verb|numericRank| function is called to set and get the numeric rank of the
tiles, i.e, \verb|Tij.numericRank(rank)| and \verb|rank = Tij.numericRank()|.
The \verb|numericRankBcast| is used to broadcast the numeric rank and insert
a compressed workspace tile
where the tile is needed for computations on another remote process.

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{figures/getrf_sketch}
    \caption{Example LU of matrix with low rank tiles.}
    \label{fig:getrf}
\end{figure}

\begin{algorithm}
    \scriptsize{
    \begin{algorithmic}[1]
    % todo: getrf_compressed? lu_factor_compressed?
    \Function{slate::getrf\_diag\_piv\_UV}{Matrix $A$}
        \For {$k = 0$ to $mt-1$}  %% k=0; k < mt; ++k
            \State \Comment{Compute the inverse of the diagonal tiles}
            \State lapack::getrf( $A(k, k)$ )
            \State lapack::getri( $A(k, k)$ )
            \State \Comment{Update the column panel}
            \State internal::gemm\_compressed( $A(k+1:mt-1, k), A(k, k), A(k+1:mt-1, k)$ )
            \State \Comment{Update lookahead (la) columns}
            \For {$j = k+1$ to $\min(k+la, nt-1)$}  %% j=k+1; j < k+1+la && j < nt; ++j
                \State internal::gemm\_compressed( $A(k+1:mt-1, k), A(k, j), A(k+1:mt-1, j)$ )
            \EndFor
            \State \Comment{Update trailing submatrix}
            \For {$k+1+la < nt$}
                \State internal::gemm\_compressed( $A(k+1:mt-1, k), A(k, k+1+la:nt), A(k+1:mt-1, k+1+la:nt)$ )
            \EndFor
        \EndFor
    \EndFunction
    \end{algorithmic}
    } % scriptsize
    \caption{The low-rank restricted pivoting LU.}
    \label{alg:getrf}
\end{algorithm}

%----------------------------------------
\section{Low-rank Matrix-matrix Multiplication}
\label{sec:internal-gemm-comp}

The \verb|internal::gemm_compressed| routine, shown in \cref{alg:gemm-comp},
performs matrix-matrix multiplication of low-rank tiles.
Where the internal routines generally perform one step or major task of a computational routine,
these are typically executed in parallel across multiple CPU cores.
Most internal routines consist of a set of independent tile operations that can be issued as a batch
gemm or an OpenMP parallel-for loop, with no task dependencies to track.

The \verb|internal::gemm_compressed| routine loops over all tiles in the matrix C,
and selects just the local tiles to operate on through the call to the tile routine \verb|tile::gemm_compressed|.
Tile routines update one tile, generally sequentially on a single CPU core.
The different variants to fully support the low-rank LU based solver
are illustrated in \cref{alg:tile-gemm-comp}.

The \verb|tileUpdateCompressed| function can be used to update the numeric rank and the allocation of the tiles,
which may change during the \verb|internal::gemm_compressed| computations.
The randomized SVD (rSVD) is invoked in a few cases to re-compress tiles that
have grown in numeric rank.

\begin{algorithm}
    \scriptsize{
    \begin{algorithmic}
    \Function{internal::gemm\_compressed}{Matrix $A, B, C$}
        \For {$i = 0$ to $C.mt - 1$}
            \For {$j = 0$ to $C.nt - 1$}
                \If {$C(i, j)$ is local}
                    \State tile::gemm\_compressed( $A(i, 0), B(0, j), C(i, j)$ )
                \EndIf
            \EndFor
        \EndFor
    \EndFunction
    \end{algorithmic}
    } % scriptsize
    \caption{The low-rank internal matrix-matrix multiplication
             performs block outer product $C = C + AB$,
             where $A$ is one block column and $B$ is one block row.}
    \label{alg:gemm-comp}
\end{algorithm}

\begin{algorithm}
    \scriptsize{
    \begin{algorithmic}
    \Function{tile::gemm\_compressed}{Tile $A, B, C$}
        \State \Comment{$A = U_A \times V_A$}
        \State \Comment{$B = U_B \times V_B$}
        \State \Comment{$C = U_C \times V_C$}
        \If {($A$ is full and $B$ is full)}
            \If {($C$ is compressed)}
                \State \Comment{call ACA to recompress}
            \Else
                %\State \Comment{C = alpha A B + beta C}
                \State blas::gemm( $U_A, U_B, U_C$ )
            \EndIf
        \ElsIf {($A$ is full and $B$ is compressed)}
            %\State \Comment{$W = alpha A \times UB$}
            \State blas::gemm( $U_A, U_B, W_1$ )
            \If {($C$ is compressed)}
                %\State \Comment{$U_c’ V_c’ = rsvd( W V_B + beta U_C V_C )$}
                \State rsvd( $W_1, V_B, U_C, V_C,$ \&new\_rank, \&new\_C )
                %\State \Comment{Update the C pointer and numeric rank}
                \State $C$.data( new\_C )
                \State $C$.numericRank( new\_rank )
            \Else
                %\State \Comment{C = W V_B + beta C}
                \State blas::gemm( $W_1, V_B, U_C$ )
            \EndIf
        \ElsIf {($A$ is compressed and $B$ is full)}
            \If {($C$ is compressed)}
                %\State \Comment{W = V_A B}
                \State blas::gemm( $V_A, U_B, W_1$ )
                %\State \Comment{newUC newVC = rsvd( UA W + beta UC VC )}
                \State rsvd( $U_A, W_1, U_C, V_C,$ \&new\_rank, \&new\_C )
                \State \Comment{Update the C pointer and numeric rank}
                \State $C$.data( new\_C )
                \State $C$.numericRank( new\_rank )
            \Else
                %\State \Comment{W = V_A B}
                \State blas::gemm( $V_A, U_B, W_1$ )
                %\State \Comment{C = U_A W}
                \State blas::gemm( $U_A, W_1, U_C$ )

            \EndIf
        \ElsIf {$A$ is compressed and $B$ is compressed)}
            \State blas::gemm( $V_A, U_B, W_1$ )
            \If {($\rank(A) \le \rank(B)$)}
                \State blas::gemm( $W_1, V_B, W_2$ )
            \Else
                \State blas::gemm( $U_A, W_1, W_2$ )
            \EndIf
            \If {($C$ is compressed)}
                \If {($\rank(A) \le \rank(B)$)}
                    \State rsvd( $U_A, W_2, U_C, V_C,$ \&new\_rank, \&new\_$C$ )
                \Else
                    \State rsvd( $W_2, V_B, U_C, V_C,$ \&new\_rank, \&new\_$C$ )
                \EndIf
                \State \Comment{Update the C pointer and numeric rank}
                \State $C$.data( new\_C )
                \State $C$.numericRank( new\_rank )
            \Else
                \If {($\rank(A) \le \rank(B)$)}
                    \State blas::gemm( $U_A, W_2, U_C$ )
                \Else
                    \State blas::gemm( $W_2, V_B, U_C$ )
                \EndIf
            \EndIf
        \EndIf
    \EndFunction
    \end{algorithmic}
    } % scriptsize
    \caption{The low-rank tile matrix-matrix multiplication.}
    \label{alg:tile-gemm-comp}
\end{algorithm}

%----------------------------------------
\section{Solving Linear System}
\label{sec:getrs}

After the lower and upper low-rank factors $L$ and $U$ are computed through the call to
\verb|getrf_diag_piv_UV| routine,
the system $Ax=B$ can be solved by forward and backward substitutions using \verb|getrs_diag_piv|.
The \verb|getrs_diag_piv| routine relies on \verb|trsm_diag_piv| to solve the
triangular systems, as shown in \cref{alg:getrs}.

\Cref{alg:trsm} presents the implementation details of \verb|trsm_diag_piv| to solve
the triangular systems $Ly=b$ and $Ux=y$, where $L$ and $U$ are low rank matrices.
Since the diagonal tiles have already been inverted, the entire computation
is matrix multiply calls (\verb|gemm|).

\begin{algorithm}
    \scriptsize{
    \begin{algorithmic}
    \Function{slate::trsm\_diag\_piv}{Matrix $A, B$}
        \State \Comment{$A = LU$}
        \State \Comment{Forward substitution $Y = L^{-1} B$, $Y$ overwrites $B$}
        \State slate::trsm\_diag\_piv( $L, B$ )  %% Left
        \State \Comment{Backward substitution $X = U^{-1} Y$, $X$ overwrites $B$}
        \State slate::trsm\_diag\_piv( $U, B$ )  %% Left
    \EndFunction
    \end{algorithmic}
    } % scriptsize
    \caption{Solving the low-rank linear system.}
    \label{alg:getrs}
\end{algorithm}


\begin{algorithm}
    \scriptsize{
    \begin{algorithmic}
    \Function{slate::trsm\_diag\_piv}{Matrix $A, B$}
        \If {$A$.uplo() = Lower}
            \For {$k = 0$ to $mt-1$}  %% k = 0; k < mt; ++k
                %\State \Comment{send $A(i=k+1:mt-1, k)$ to ranks owning block row $B(i, :)$}
                %\State \Comment{send $B(k, j=0:nt-1)$ to ranks owning $B(k+1:mt-1, :)$}
                \State \Comment{Update lookahead (la) columns}
                \For {$i = k+1$ to $\min( k+la, mt-1 )$}  %% i = k+1; i < k+1+la && i < mt; ++i
                    \State internal::gemm\_compressed( $A(i, k), B(k, 0:nt-1), B(i, 0:nt-1)$ )
                \EndFor
            \EndFor
            \State \Comment{Update trailing submatrix}
            \If {$(k+1+la < mt)$}
                \State internal::gemm\_compressed( $A(k+1+la:mt-1, k), B(k, 0:nt-1), B(k+1+la:mt-1, 0:nt-1)$ )
            \EndIf

        \Else
            \For {$k = mt-1$ down to $0$}  %% k = mt-1; k \ge 0; --k
                %\State \Comment{send $A(k, k)$ to ranks owning block row $B(k, :)$}
                \State internal::gemm\_compressed( $A(k, k), B(k, 0:nt-1), B(k, 0:nt-1)$ )
                %\State \Comment{send $A(i=0:k-1, k)$ to ranks owning block row $B(i, :)$}
                %\State \Comment{send $B(k, j=0:nt-1)$ to ranks owning block col $B(0:k-1, j)$}
                \State \Comment{Update lookahead (la) columns}
                \For {$i = k-1$ down to $0$}
                    \State internal::gemm\_compressed( $A(i, k), B(k, 0:nt-1), B(i, 0:nt-1)$ )
                \EndFor
                \State \Comment{Update trailing submatrix}
                \If {$(k-1-la \ge 0)$}
                    \State internal::gemm\_compressed( $A(0:k-1-la, k), B(k, 0:nt-1), B(0:k-1-la, 0:nt-1)$ )
                \EndIf
            \EndFor
        \EndIf
    \EndFunction
    \end{algorithmic}
    } % scriptsize
    \caption{Solving the low-rank triangular systems.}
    \label{alg:trsm}
\end{algorithm}

%===============================================================================
%\chapter{Numerical Results and Analysis}
%\label{ch:perf}
% how we generate random test matrices. Full diagonal tiles, low rank off-diagonal tiles
% metrics to check the accuracy results
% accuracy results
% performance/scalability results on Summit
%===============================================================================
%\section{Next Steps}
%\label{sec:next}
% GPU support
% more general cases other than Full diagonal tiles and low rank off-diagonal tiles
% call ACA to deal with F*F + C = C
%===============================================================================

\end{document}




